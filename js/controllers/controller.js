export let showDataGlasses = (list) => {
  let contentHTML = "";
  list.forEach((item) => {
    contentHTML += `
        <div class="col-4">
        <button class="btn" onclick="themKinh('${item.id}')">
        <img class="img-fluid" src="${item.src}">
        </button>
    </div>`;
  });
  document.getElementById("vglassesList").innerHTML = contentHTML;
};

export let renderGlassChose = (glassEdit) => {
  let avatar = `<img id="virtualImg" class="img-fluid" src="${glassEdit.virtualImg}">`;
  document.getElementById("avatar").innerHTML = avatar;

  let glassesInfo = `<div>
  <h4>${glassEdit.name} - ${glassEdit.brand}</h4>
  <span class="btn btn-danger">$ ${glassEdit.price}</span>
  <span> (${glassEdit.color})</span>
  <p>${glassEdit.description}</p>
  </div>`;
  document.getElementById("glassesInfo").innerHTML = glassesInfo;
};
